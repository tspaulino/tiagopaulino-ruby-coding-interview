class Tweet < ApplicationRecord
  before_create :validate_duplicated_content
  belongs_to :user


  def validate_duplicated_content
    Tweet.where(body: self.body, created_at: 1.day.ago..Time.now).empty?
  end
end
